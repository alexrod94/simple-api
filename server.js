const books = require("./books.js");
const themes = require("./themes.js");
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.use(cors());

app.get("/themes", (req, res) => {
  const result = themes.list();
  if (result.ok) {
    res.status(200);
    res.json({ data: result.data });
  } else {
    res.status(400);
    res.send();
  }
});

app.get("/books", (req, res) => {
  const result = books.list();
  if (result.ok) {
    res.status(200);
    res.json({ data: result.data });
  } else {
    res.status(400);
    res.send();
  }
});

app.get("/books/:id", (req, res) => {
  const result = books.get(req.params.id);
  if (result.ok) {
    res.status(200);
    res.json({ data: result.data });
  } else {
    res.status(400);
    res.send();
  }
});

app.post("/books", (req, res) => {
  console.log(req.body);
  const result = books.add(req.body);
  if (result.ok) {
    res.status(200);
    res.json({ id: result.id });
  } else {
    res.status(400);
    res.send();
  }
});

app.put("/books/:id", (req, res) => {
  const result = books.edit(req.params.id, req.body);
  if (result.ok) {
    res.status(200);
    res.send();
  } else {
    res.status(400);
    res.send();
  }
});

app.delete("/books/:id", (req, res) => {
  const result = books.del(req.params.id);
  if (result.ok) {
    res.status(200);
    res.send();
  } else {
    res.status(400);
    res.send();
  }
});

app.get("/themes/:id", (req, res) => {
  const result = themes.get(req.params.id);
  if (result.ok) {
    res.status(200);
    res.json({ data: result.data });
  } else {
    res.status(400);
    res.send();
  }
});

app.post("/themes", (req, res) => {
  const result = themes.add(req.body);
  if (result.ok) {
    res.status(200);
    res.json({ id: result.id });
  } else {
    res.status(400);
    res.send();
  }
});

app.put("/themes/:id", (req, res) => {
  const result = themes.edit(req.params.id, req.body);
  if (result.ok) {
    res.status(200);
    res.send();
  } else {
    res.status(400);
    res.send();
  }
});

app.delete("/themes/:id", (req, res) => {
  const result = themes.del(req.params.id);
  if (result.ok) {
    res.status(200);
    res.send();
  } else {
    res.status(400);
    res.send();
  }
});

app.listen(3001);
