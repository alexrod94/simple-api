let books = [
  {
    id: 1,
    title: "Book A",
    pages: 1000,
    type: "religion"
  },
  {
    id: 2,
    title: "Book B",
    pages: 500,
    type: "fiction"
  },
  {
    id: 3,
    title: "Book C",
    pages: 250,
    type: "adventure"
  }
];
const themes = require("./themes");

function add(data = {}) {
  let check = true;
  if (typeof data.id !== "undefined") check = false;
  if (typeof data.title !== "string") check = false;
  if (!(typeof data.pages === "undefined" || typeof data.pages === "number"))
    check = false;
  if (typeof data.type !== "string") check = false;
  const comprobar = themes.checkTheme(data.type);
  if (themes.checkTheme(data) === false) check = false;
  if (!check) {
    return { ok: false };
  }
  const id = books.length;
  const newBook = {
    id: id,
    title: data.title,
    pages: data.pages,
    type: data.type
  };
  books.push(newBook);
  return {
    ok: true,
    id: id
  };
}

function get(id) {
  if (books[id]) {
    return { ok: true, data: books[id] };
  }
  return { ok: false };
}

function del(id) {
  if (!books[id]) {
    return { ok: false };
  }
  delete books[id];
  return { ok: true };
}

function list(criteria = () => true) {
  return { ok: true, data: books.filter(criteria) };
}

function edit(id, data) {
  if (!books[id]) return { ok: false };
  books[id] = Object.assign(books[id], data);
  if (themes.checkTheme(books[id]) === false) return { ok: false };
  return { ok: true };
}

module.exports = { add, del, list, edit, get };
