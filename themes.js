let themes = [
  {
    id: 1,
    type: "religion"
  },
  {
    id: 2,
    type: "fiction"
  },
  {
    id: 3,
    type: "adventure"
  }
];

function add(data = {}) {
  let check = true;
  if (typeof data.id !== "undefined") check = false;
  if (typeof data.type !== "string") check = false;
  if (!check) {
    return { ok: false };
  }
  const id = themes.length;
  const newTheme = {
    id: id,
    type: data.type
  };
  themes.push(newTheme);
  return {
    ok: true,
    id: id
  };
}

function checkTheme(newTheme) {
  const result = themes.find(theme => theme.type === newTheme.type);
  if (typeof result === "undefined") return false;
  return true;
}

function get(id) {
  if (themes[id]) {
    return { ok: true, data: themes[id] };
  }
  return { ok: false };
}

function del(id) {
  if (!themes[id]) {
    return { ok: false };
  }
  delete themes[id];
  return { ok: true };
}

function list(criteria = () => true) {
  return { ok: true, data: themes.filter(criteria) };
}

function edit(id, data) {
  if (!themes[id]) return { ok: false };
  themes[id] = Object.assign(themes[id], data);
  return { ok: true };
}

module.exports = { add, del, list, edit, get, checkTheme };
