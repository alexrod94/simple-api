const expect = require("chai").expect;
const fetch = require("node-fetch");
const HOST = "http://localhost:3001";
const themes = require("../themes");

describe("Books", function() {
  this.timeout(10000);

  describe("get", function() {
    it("should return a book list", function(done) {
      fetch(HOST + "/books")
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.data).to.be.an("array");
          done();
        });
    });
    it("should return a book", async function() {
      const newRes = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "religion" })
      });
      const data = { title: "My Book", type: "religion", pages: 1024 };

      const res = await fetch(HOST + "/books", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });

      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);
    });
  });

  describe("post", function() {
    it("should return a send book", function(done) {
      const newRes = fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "religion" })
      });
      const data = { title: "My Book", type: "religion", pages: 1024 };
      fetch(HOST + "/books", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      })
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.id).to.be.an("number");
          return fetch(HOST + "/books/" + result.id, {
            method: "GET"
          });
        })
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.data.title).to.be.equal("My Book");
          done();
        });
    });
    it("should return a send book by async/await", async function() {
      const newRes = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "religion" })
      });
      const data = { title: "My Book", type: "religion", pages: 1024 };

      const res = await fetch(HOST + "/books", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);

      const result2 = await res2.json();
      expect(result2.data.title).to.be.equal("My Book");
    });
  });

  describe("put", function() {
    it("should edit book by async/await", async function() {
      const newRes = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "religion" })
      });
      const data = { title: "My Book", type: "religion", pages: 1024 };

      const res = await fetch(HOST + "/books", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);
      const result2 = await res2.json();
      expect(result2.data.title).to.be.equal("My Book");
      expect(result2.data.pages).to.be.equal(1024);

      const res3 = await fetch(HOST + "/books/" + result.id, {
        method: "PUT",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ pages: 1000 })
      });
      expect(res3.status).to.be.equal(200);

      const res4 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res4.status).to.be.equal(200);

      const result4 = await res4.json();
      expect(result4.data.pages).to.be.equal(1000);
    });
  });

  describe("delete", function() {
    it("should delete a book", async function() {
      const newRes = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "religion" })
      });
      const data = { title: "My Book", type: "religion", pages: 1024 };

      const res = await fetch(HOST + "/books", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);

      const res3 = await fetch(HOST + "/books/" + result.id, {
        method: "DELETE"
      });
      expect(res3.status).to.be.equal(200);

      const res4 = await fetch(HOST + "/books/" + result.id, {
        method: "GET"
      });
      expect(res4.status).to.be.equal(400);
    });
  });
});

describe("Themes", function() {
  this.timeout(10000);

  describe("get", function() {
    it("should return a themes list", function(done) {
      fetch(HOST + "/themes")
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.data).to.be.an("array");
          done();
        });
    });
    it("should return a theme", async function() {
      const data = { type: "My Theme" };

      const res = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.a("number");

      const res2 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);
    });
  });

  describe("post", function() {
    it("should return a send theme", function(done) {
      const data = { type: "My Theme" };
      fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      })
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.id).to.be.an("number");
          return fetch(HOST + "/themes/" + result.id, {
            method: "GET"
          });
        })
        .then(res => {
          expect(res.status).to.be.equal(200);
          return res.json();
        })
        .then(result => {
          expect(result.data.type).to.be.equal("My Theme");
          done();
        });
    });
    it("should return a send theme by async/await", async function() {
      const data = { type: "My Theme" };

      const res = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.a("number");

      const res2 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);

      const result2 = await res2.json();
      expect(result2.data.type).to.be.equal("My Theme");
    });
  });

  describe("put", function() {
    it("should edit theme by async/await", async function() {
      const data = { type: "My Theme" };

      const res = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);

      const result2 = await res2.json();
      expect(result2.data.type).to.be.equal("My Theme");

      const res3 = await fetch(HOST + "/themes/" + result.id, {
        method: "PUT",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ type: "My new theme" })
      });
      expect(res3.status).to.be.equal(200);

      const res4 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res4.status).to.be.equal(200);

      const result4 = await res4.json();
      expect(result4.data.type).to.be.equal("My new theme");
    });
  });

  describe("delete", function() {
    it("should delete a theme", async function() {
      const data = { type: "My theme to be deleted" };

      const res = await fetch(HOST + "/themes", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(data)
      });
      expect(res.status).to.be.equal(200);

      const result = await res.json();
      expect(result.id).to.be.an("number");

      const res2 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res2.status).to.be.equal(200);

      const res3 = await fetch(HOST + "/themes/" + result.id, {
        method: "DELETE"
      });
      expect(res3.status).to.be.equal(200);

      const res4 = await fetch(HOST + "/themes/" + result.id, {
        method: "GET"
      });
      expect(res4.status).to.be.equal(400);
    });
  });
});
