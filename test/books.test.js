const expect = require("chai").expect;
const core = require("../books.js");
const themes = require("../themes");

describe("core books", function() {
  it("should add a book", function() {
    themes.add({ type: "religion" });
    const result = core.add({ title: "the holy bible", type: "religion" });
    expect(result.ok).to.be.equal(true);
    const ret = core.get(result.id);
    expect(ret.data.title).to.be.equal("the holy bible");
  });
  it("should del a book", function() {
    const result = core.add({ title: "the holy bible", type: "religion" });
    expect(result.ok).to.be.equal(true);
    const ret = core.get(result.id);
    expect(ret.data.title).to.be.equal("the holy bible");
    const resultDel = core.del(result.id);
    const retDel = core.get(result.id);
    expect(retDel.ok).to.be.equal(false);
  });
  it("should edit a book", function() {
    themes.add({ type: "religion" });
    const result = core.add({
      title: "the holy bible",
      type: "religion",
      pages: 1
    });
    expect(result.ok).to.be.equal(true);
    let ret = core.get(result.id);
    expect(ret.data.title).to.be.equal("the holy bible");
    const resultEdit = core.edit(result.id, { type: "fiction" });
    expect(resultEdit.ok).to.be.equal(false);
    themes.add({ type: "fiction" });
    const newEdit = core.edit(result.id, { type: "fiction" });
    expect(newEdit.ok).to.be.equal(true);
  });
  it("should get a book", function() {
    const result = core.add({ title: "the holy bible", type: "religion" });
    expect(result.ok).to.be.equal(true);
    const ret = core.get(result.id);
    expect(ret.data.title).to.be.equal("the holy bible");
  });
  it("should list books", function() {
    const result = core.add({ title: "the holy bible", type: "religion" });
    expect(result.ok).to.be.equal(true);
    const ret = core.list();
    expect(ret.data).to.be.an("array");
  });
});
