const expect = require("chai").expect;
const themes = require("../themes");

describe("Testing the themes functions", function() {
  this.timeout(10000);
  describe("Testing the add function", function() {
    it("should add a theme", function() {
      const result = themes.add({ type: "fiction" });
      expect(result.ok).to.be.equal(true);
    });
  });

  describe("Testing the get function", function() {
    it("should get one theme", function() {
      const add = themes.add({ type: "fiction" });
      const id = themes.list();
      const result = themes.get(id.data.length - 1);
      expect(result.ok).to.be.equal(true);
      expect(result.data.type).to.be.equal("fiction");
    });
  });

  describe("Testing the del function", function() {
    it("should delete one theme", function() {
      const add = themes.add({ type: "fiction" });
      const result = themes.del(add.id);
      expect(result.ok).to.be.equal(true);
      const delResult = themes.get(add.id);
      expect(delResult.ok).to.be.equal(false);
    });
  });

  describe("Testing the list function", function() {
    it("should return an array of themes", function() {
      const result = themes.add({ type: "fiction" });
      expect(result.ok).to.be.equal(true);
      const ret = themes.list();
      expect(ret.data).to.be.an("array");
    });
  });

  describe("Testing the edit function", function() {
    it("should edit a theme", function() {
      const result = themes.add({ type: "fiction" });
      const update = themes.edit(0, { type: "adventure" });
      expect(update.ok).to.be.equal(true);
      const check = themes.get(0);
      expect(check.ok).to.be.equal(true);
      expect(check.data.type).to.be.equal("adventure");
    });
  });
});
